# How to install the code #
[Detailed Tutorial](http://www.expertshopify.com/shopify-code-snippets/shopify-venture-theme-related-products/)

1. Log in to your Shopify Admin

2. Navigate to **Online Store** from the side bar

3. Click on **Themes**

4. Your current theme is on the top of the page. Click the ... button to drop down the menu and select **Edit HTML/CSS**

5. Open your **theme.liquid** file and scroll to the bottom of the page.

6. Just above the `</body>` tag, copy and Paste code from [shopify_external_links_new_tab.js](
shopify_external_links_new_tab.js) or code below in to the file and save

    <script>

      jQuery('a[href^="http"]').not('a[href^="{{ shop.url }}"]').attr('target', '_blank');

    </script>