Readme.md
# Intro #

Here are a few useful snippets of code that I have created/found while developing shopify stores. They are normally part of a larger tutorial on [www.expertshopify.com](http://www.expertshopify.com)

# Useful Tools #
* [Open external links in new tab](Open External Links in New Tab)

# Theme Specific #

## Venture Theme ##
* [Related Products](Venture Theme Related Products)