# How to install the code #
[Detailed Tutorial](http://www.expertshopify.com/shopify-code-snippets/shopify-venture-theme-activate-megamenu-when-mouse-hovers-over-links/)
1. Log in to your Shopify Admin
2. Navigate to **Online Store** from the side bar
3. Click on **Themes**
4. Your current theme is on the top of the page. Click the ... button to drop down the menu and select **Edit HTML/CSS**
5. Create a new asset called expertshopify.js.
6. Copy and Paste code from [mouseover-menu.js](mouseover-menu.js)
7. Edit the product.liquid file and add `<script src="{{ 'expertshopify.js' | asset_url }}"></script>` just above the `</body>`

