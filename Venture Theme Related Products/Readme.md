# How to install the code #
[Detailed Tutorial](http://www.expertshopify.com/shopify-code-snippets/shopify-venture-theme-related-products/)
1. Log in to your Shopify Admin
2. Navigate to **Online Store** from the side bar
3. Click on **Themes**
4. Your current theme is on the top of the page. Click the ... button to drop down the menu and select **Edit HTML/CSS**
5. Create a new snippet called related-products.
6. Copy and Paste code from [related-products.liquid](related-products.liquid)
7. Edit the product.liquid file and add `{% include 'related-products' %}` around line 141

    {% if collection %}
      {% include 'related-products' %}
